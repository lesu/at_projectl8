package personal.projectl8;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import android.widget.ImageView;


/**
 * <ol type="I">
 * <li><a href="http://developer.android.com/reference/android/graphics/BitmapFactory.Options.html">developer.android.com | BitmapFactory.Options</a></li>
 * <li><a href="http://developer.android.com/reference/android/graphics/BitmapFactory.html">developer.android.com | BitmapFactory</a></li>
 * <li><a href="http://developer.android.com/training/displaying-bitmaps/index.html">developer.android.com/training | Displaying Bitmaps Efficiently</a></li>
 * </ol>
 */
final class ImageOptimizer {




    final static void loadImageToImageView(Context context, ImageView imageView, int srcImageId) {
        //finding imageView size in pixels.
        Resources resources = context.getResources();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(resources, srcImageId, options);

        int srcWidth = options.outWidth;
        int srcHeight = options.outHeight;
        ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
        int targetWidth = layoutParams.width;
        int targetHeight = layoutParams.height;
        int inSampleSize = calculateInSampleSize(context,srcWidth, srcHeight, targetWidth, targetHeight);

        options.inSampleSize = inSampleSize;
//        Toast.makeText(context,"size : "+inSampleSize,Toast.LENGTH_SHORT).show(); //debug line
        options.inJustDecodeBounds = false;
        Bitmap bitmap = BitmapFactory.decodeResource(resources, srcImageId, options);
        imageView.setImageBitmap(bitmap);

    }

    final static int calculateInSampleSize(Context context,int srcWidth, int srcHeight, int targetWidth, int targetHeight) {
        int inSampleSize = 1;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int displayWidth = displayMetrics.widthPixels;
        int displayHeight = displayMetrics.heightPixels;
        if(targetWidth<1) {
            targetWidth = displayWidth;
        }
        if(targetHeight<1) {
            targetHeight = displayHeight;
        }
        float widthRatio = (float)srcWidth/(float)targetWidth;
        float heightRatio = (float)srcHeight/(float)targetHeight;
        if(widthRatio>heightRatio) {
            inSampleSize = Math.round(widthRatio);
        } else {
            inSampleSize = Math.round(heightRatio);
        }
        return inSampleSize;
    }



}
