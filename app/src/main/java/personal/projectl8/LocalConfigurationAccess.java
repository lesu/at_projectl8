package personal.projectl8;


import android.content.Context;
import android.content.SharedPreferences;

final class LocalConfigurationAccess {
    final static int SP_PRIVATE_MODE = Context.MODE_PRIVATE;
    final static String SP_CONFIG_FOLDER_NAME = "config";
    final static String SP_KEY_RATING = "rating";
    final static String SP_KEY_REMEMBER_ME = "remember";
    final static float SP_RATING_VALUE_NO_RATING = 0f;
    final static boolean SP_REMEMBER_VALUE_NO_REMEMBER = false;


    static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(SP_CONFIG_FOLDER_NAME, SP_PRIVATE_MODE);
    }

    static void setRememberMe(Context context,boolean rememberMe) {
        SharedPreferences sp = getSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(SP_KEY_REMEMBER_ME,rememberMe);
        editor.commit();
    }

    static void setRating(Context context, float rating) {
        SharedPreferences sp = getSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putFloat(SP_KEY_RATING, rating);
        editor.commit();
    }

    static boolean getRememberMe(Context context) {
        SharedPreferences sp = getSharedPreferences(context);
        return sp.getBoolean(SP_KEY_REMEMBER_ME,SP_REMEMBER_VALUE_NO_REMEMBER);
    }

    static float getRating(Context context) {
        SharedPreferences sp = getSharedPreferences(context);
        return sp.getFloat(SP_KEY_RATING,SP_RATING_VALUE_NO_RATING);
    }
}
