package personal.projectl8;


import android.app.Activity;

import java.util.ArrayList;

class Exit {
    private static ArrayList<Activity> runningActivities = new ArrayList<Activity>();

    static void addRunningActivity(Activity activity) {
        Exit.runningActivities.add(activity);
    }

    static void exit(){
        for (int i = 0; i < runningActivities.size(); i++) {
            Exit.runningActivities.get(i).finish();
        }
    }

}
