package personal.projectl8;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class Login extends AppCompatActivity {
    private EditText editTextUserName;
    private EditText editTextPassword;
    private Button buttonLogin;
    private Button buttonSignup;
    private CheckBox checkBoxRememberMe;
    private ImageView imageView;
    private final static String INCORRECT_USER_INFO = "Incorrect username or password";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Exit.addRunningActivity(this);
        this.initComponents();
    }


    private void initComponents() {
        this.editTextUserName = (EditText) this.findViewById(R.id.login_edit_text_user_name);
        this.editTextPassword = (EditText) this.findViewById(R.id.login_edit_text_password);
        this.buttonLogin = (Button) this.findViewById(R.id.login_button_login);
        this.buttonSignup = (Button) this.findViewById(R.id.login_button_signup);
        this.checkBoxRememberMe = (CheckBox) this.findViewById(R.id.login_check_box_remember_me);
        this.imageView = (ImageView) this.findViewById(R.id.login_image_view);

        ImageOptimizer.loadImageToImageView(this,this.imageView,R.drawable.login);

        this.buttonSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Login.this.startActivity(new Intent(Login.this,Signup.class));
            }
        });

        this.buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = Login.this.editTextUserName.getText().toString();
                String password = Login.this.editTextPassword.getText().toString();

                if(VirtualDBAccess.validateCredential(Login.this,userName,password)) {
                    if(Login.this.checkBoxRememberMe.isChecked()) {
                        LocalConfigurationAccess.setRememberMe(Login.this.getApplicationContext(),true);
                    } else {
                        LocalConfigurationAccess.setRememberMe(Login.this.getApplicationContext(),false);
                    }
                    Login.this.startActivity(new Intent(Login.this, Contacts.class));
                    Login.this.finish();
                } else {
                    Toast.makeText(Login.this,Login.INCORRECT_USER_INFO,Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

}
