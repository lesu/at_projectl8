package personal.projectl8;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class Contacts extends AppCompatActivity {
    private EditText editTextUserName;
    private EditText editTextPhone;
    private EditText editTextEmail;
    private Button buttonAddUser;
    private ArrayList<User> users = new ArrayList<User>();
    private ListView listViewUsers;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
        Exit.addRunningActivity(this);
        this.initComponents();
    }


    private void initComponents() {
        this.editTextUserName = (EditText) this.findViewById(R.id.contacts_edit_text_user_name);
        this.editTextPhone = (EditText) this.findViewById(R.id.contacts_edit_text_phone);
        this.editTextEmail = (EditText) this.findViewById(R.id.contacts_edit_text_email);
        this.buttonAddUser = (Button) this.findViewById(R.id.contacts_button_add);
        this.listViewUsers = (ListView) this.findViewById(R.id.contacts_list_view_user_info_list);

        this.buttonAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = Contacts.this.editTextUserName.getText().toString();
                String phone = Contacts.this.editTextPhone.getText().toString();
                String email = Contacts.this.editTextEmail.getText().toString();
                Contacts.this.editTextUserName.setText("");
                Contacts.this.editTextPhone.setText("");
                Contacts.this.editTextEmail.setText("");
                try {
                    Contacts.this.users.add(new User(userName,phone,email));
                } catch (InvalidUserException e) {
                    Toast.makeText(Contacts.this.getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
                    Log.e("InvalidUserException",e.getMessage(),e);
                }

                UserInfoAdapter adapter = new UserInfoAdapter(Contacts.this.getApplicationContext(),Contacts.this.users);
                Contacts.this.listViewUsers.setAdapter(adapter);
            }
        });

        this.listViewUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User user = (User) Contacts.this.listViewUsers.getItemAtPosition(position);
                Intent intent = new Intent(Contacts.this,Details.class);
                intent.putExtra(Details.INTENT_EXTRA_USERNAME_NAME,user.getUserName());
                intent.putExtra(Details.INTENT_EXTRA_PHONE_NAME,user.getPhone());
                intent.putExtra(Details.INTENT_EXTRA_EMAIL_NAME,user.getEmail());
                Contacts.this.startActivity(intent);
            }
        });

    }

}
