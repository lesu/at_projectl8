package personal.projectl8;

import java.nio.charset.Charset;

final class User {
    private String userName;
    private String phone;
    private String email;


    public User(String userName, String phone, String email) throws InvalidUserException {
        this.setUserName(userName);
        this.setPhone(phone);
        this.setEmail(email);
    }

    public String getUserName() {
        return userName;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }


    public void setUserName(String userName) throws InvalidUserException {
        if(validateUserName(userName)) {
            this.userName = userName;
        } else {
            throw new InvalidUserException(InvalidUserException.INVALID_USER_NAME_MESSAGE);
        }
    }

    public void setPhone(String phone) throws InvalidUserException {
        if(validatePhone(phone)) {
            this.phone = phone;
        } else {
            throw new InvalidUserException(InvalidUserException.INVALID_PHONE_MESSAGE);
        }
    }

    public void setEmail(String email) throws InvalidUserException {
        if(validateEmail(email)) {
            this.email = email;
        } else {
            throw new InvalidUserException(InvalidUserException.INVALID_EMAIL_MESSAGE);
        }
    }

    //Validation Methods.
    private static boolean validateUserName(String userName) {
        return true;
    }

    private static boolean validatePhone(String phone) {
        return true;
    }

    private static boolean validateEmail(String email) {
        return true;
    }

    private static boolean isASCIIPrintableOnly(String text) {
        if( !(text.matches("[ -~]+]"))   ) {
            return false;
        }
        return Charset.forName("US-ASCII").newEncoder().canEncode(text);
    }

    private static boolean isDigitsOnly(String text) {
        return text.matches("[0-9]+");
    }

    private static boolean isDigitsAndLettersOnly(String text) {
        return text.matches("[0-9A-Za-z]+");
    }

}
