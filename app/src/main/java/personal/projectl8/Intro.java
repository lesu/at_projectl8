package personal.projectl8;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

public class Intro extends AppCompatActivity {
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        Exit.addRunningActivity(this);
        this.init();
    }

    private void init() {
        this.imageView = (ImageView) this.findViewById(R.id.intro_image_view);
        ImageOptimizer.loadImageToImageView(this,this.imageView,R.drawable.design);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                boolean rememberMe = LocalConfigurationAccess.getRememberMe(Intro.this.getApplicationContext());
                if(rememberMe) {
                    Intro.this.startActivity(new Intent(Intro.this,Contacts.class));
                    Intro.this.finish();
                } else {
                    Intro.this.startActivity(new Intent(Intro.this,Login.class));
                    Intro.this.finish();
                }
            }
        }, 2000);
    }

}
