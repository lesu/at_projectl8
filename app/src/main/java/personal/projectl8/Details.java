package personal.projectl8;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class Details extends AppCompatActivity {
    private TextView textViewUserName;
    private TextView textViewPhone;
    private TextView textViewEmail;
    private Button buttonExit;
    private Intent intent;
    private ImageView imageView;

    static final String INTENT_EXTRA_USERNAME_NAME = "username";
    static final String INTENT_EXTRA_PHONE_NAME = "phone";
    static final String INTENT_EXTRA_EMAIL_NAME = "email";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Exit.addRunningActivity(this);
        this.initComponents();
    }



    private void initComponents() {
        this.textViewUserName = (TextView) this.findViewById(R.id.details_text_view_user_name);
        this.textViewPhone = (TextView) this.findViewById(R.id.details_text_view_phone);
        this.textViewEmail = (TextView) this.findViewById(R.id.details_text_view_email);
        this.buttonExit =(Button) this.findViewById(R.id.details_button_exit);
        this.intent = this.getIntent();
        this.imageView = (ImageView) this.findViewById(R.id.details_image_view);

        ImageOptimizer.loadImageToImageView(this,this.imageView,R.drawable.user);

        this.textViewUserName.setText(this.intent.getStringExtra(Details.INTENT_EXTRA_USERNAME_NAME));
        this.textViewPhone.setText(this.intent.getStringExtra(Details.INTENT_EXTRA_PHONE_NAME));
        this.textViewEmail.setText(this.intent.getStringExtra(Details.INTENT_EXTRA_EMAIL_NAME));

        this.textViewPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Details.this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("tel:" + Details.this.textViewPhone.getText().toString())));
            }
        });

        this.textViewEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = Details.this.textViewEmail.getText().toString();
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("message/rfc822");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
                Details.this.startActivity(Intent.createChooser(intent,"Email"));
            }
        });

        this.buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = (LayoutInflater) Details.this.getSystemService(LAYOUT_INFLATER_SERVICE);
                View view = inflater.inflate(R.layout.dialog_text,null);
                final EditText editTextDialog = (EditText) view.findViewById(R.id.edit_text_exit_dialog);

                AlertDialog.Builder builder = new AlertDialog.Builder(Details.this);
                builder.setCancelable(false);
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String response = editTextDialog.getText().toString().toLowerCase();
                        if(response.equals("yes")) {
                            Exit.exit();
                        }
                        if(response.equals("no")) {
                            dialog.cancel();
                        }
                    }
                });
                builder.setTitle("Confirm?");
                builder.setMessage("Are you sure?");
                builder.setView(view);
                builder.create().show();

            }
        });

    }

}
