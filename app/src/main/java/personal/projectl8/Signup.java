package personal.projectl8;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class Signup extends AppCompatActivity {
    private EditText editTextUserName;
    private EditText editTextPassword;
    private Button buttonLogin;
    private Button buttonCreateAccount;
    private ImageView imageView;
    private static final String ACCOUNT_CREATED_MESSAGE = "Account created";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        Exit.addRunningActivity(this);
        this.initComponents();
    }



    private void initComponents() {
        this.editTextUserName = (EditText) this.findViewById(R.id.signup_edit_text_user_name);
        this.editTextPassword = (EditText) this.findViewById(R.id.signup_edit_text_password);
        this.buttonLogin = (Button) this.findViewById(R.id.signup_button_login);
        this.buttonCreateAccount = (Button) this.findViewById(R.id.signup_button_create_account);
        this.imageView = (ImageView) this.findViewById(R.id.signup_image_view);

        ImageOptimizer.loadImageToImageView(this,this.imageView,R.drawable.signupicon);

        this.buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Signup.this.startActivity(new Intent(Signup.this,Login.class));
                Signup.this.finish();
            }
        });

        this.buttonCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = Signup.this.editTextUserName.getText().toString();
                String password = Signup.this.editTextPassword.getText().toString();
                if(VirtualDBAccess.createAccount(Signup.this,userName,password)) {
                    Toast.makeText(Signup.this,Signup.ACCOUNT_CREATED_MESSAGE,Toast.LENGTH_SHORT).show();
                    Signup.this.startActivity(new Intent(Signup.this,Login.class));
                    Signup.this.finish();
                }
            }
        });
    }

}
