package personal.projectl8;


import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

final class VirtualDBAccess {
    final static int SP_PRIVATE_MODE = Context.MODE_PRIVATE;
    final static String SP_FOLDER_NAME = "VDB";
    final static String SP_KEY_USERNAME = "username";
    final static String SP_KEY_PASSWORD = "password";
    final static String SP_USER_VALUE_NO_USER = "nouser";
    final static String SP_USER_VALUE_NO_PASS = "nopass";
    final static String INVALID_USER_INFO = "Invalid username or password";


    static boolean createAccount(Context context,String username, String password) {
        SharedPreferences sp = getSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        if(validateCredentialsOnCreate(username,password)) {
            editor.putString(SP_KEY_USERNAME,username);
            editor.putString(SP_KEY_PASSWORD,password);
            editor.commit();
            return true;
        } else {
            Toast.makeText(context,INVALID_USER_INFO,Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    static boolean validateCredential(Context context, String username, String password) {
        //Check not null
        if( (username==null)  ||  (password==null)  ){
            return false;
        }

        //Check no default value
        if( (username.equals(SP_USER_VALUE_NO_USER))  ||  (password.equals(SP_USER_VALUE_NO_PASS))  ){
            return false;
        }

        //Check not empty
        if( (username.equals(""))  ||  (password.equals(""))  ){
            return false;
        }

        SharedPreferences sp = getSharedPreferences(context);
        String userSP = sp.getString(SP_KEY_USERNAME, SP_USER_VALUE_NO_USER);
        String passSP = sp.getString(SP_KEY_PASSWORD, SP_USER_VALUE_NO_PASS);

        //Check SP values not equaling default value
        if( (userSP.equals(SP_USER_VALUE_NO_USER))   || (passSP.equals(SP_USER_VALUE_NO_PASS))  ) {
            return false;
        }

        if( (username.equals(userSP)) &&    (password.equals(passSP))  ) {
            return true;
        }

        return false;
    }


    static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(SP_FOLDER_NAME, SP_PRIVATE_MODE);
    }


    private static boolean validateCredentialsOnCreate(String username, String password) {
        //null check
        if( (username==null)    ||  (password==null)  ) {
            return false;
        }

        //emptycheck
        if( (username.equals(""))    ||  (password.equals(""))  ) {
            return false;
        }

        //Check no default value
        if( (username.equals(SP_USER_VALUE_NO_USER))  ||  (password.equals(SP_USER_VALUE_NO_PASS))  ){
            return false;
        }

        return true;
    }

}
