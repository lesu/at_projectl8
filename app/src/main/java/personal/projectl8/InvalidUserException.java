package personal.projectl8;


final class InvalidUserException extends Exception{
    static final String INVALID_USER_NAME_MESSAGE = "Invalid username";
    static final String INVALID_PHONE_MESSAGE = "Invalid phone number";
    static final String INVALID_EMAIL_MESSAGE = "Invalid email";
    static final String INVALID_USER_INFORMATION_MESSAGE = "Invalid user information";


    /**
     * Constructs a new {@code Exception} that includes the current stack trace.
     */
    public InvalidUserException() {
        super(InvalidUserException.INVALID_USER_INFORMATION_MESSAGE);
    }

    /**
     * Constructs a new {@code Exception} with the current stack trace and the
     * specified detail message.
     *
     * @param detailMessage the detail message for this exception.
     */
    public InvalidUserException(String detailMessage) {
        super(detailMessage);
    }

    /**
     * Constructs a new {@code Exception} with the current stack trace, the
     * specified detail message and the specified cause.
     *
     * @param detailMessage the detail message for this exception.
     * @param throwable
     */
    public InvalidUserException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    /**
     * Constructs a new {@code Exception} with the current stack trace and the
     * specified cause.
     *
     * @param throwable the cause of this exception.
     */
    public InvalidUserException(Throwable throwable) {
        super(throwable);
    }
}
